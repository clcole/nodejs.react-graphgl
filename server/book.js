
// create a schema
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const bookSchema = new Schema({
  name: String,
  genre: String,
  authorId: String
});

// create and export a model
module.exports = mongoose.model("Book", bookSchema);
