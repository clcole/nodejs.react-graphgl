
// create a schema
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const authorSchema = new Schema({
  name: String,
  age: Number
});

// create and export a model
module.exports = mongoose.model("Author", authorSchema);
