const Author = require("./author");
const Book = require("./book");

const {
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
  GraphQLID,
  GraphQLNonNull
} = require("graphql");

// const authors = [
//   {id: "1", name: "Author X", age: 60},
//   {id: "2", name: "Author Y", age: 70},
//   {id: "3", name: "Author Z", age: 80}
// ];

// const books = [
//   {id: "1", name: "Book 1", genre: "fiction", authorId: "1"},
//   {id: "2", name: "Book 2", genre: "non-fiction", authorId: "2"},
//   {id: "3", name: "Book 3", genre: "non-fiction", authorId: "3"},
//   {id: "4", name: "Book 4", genre: "fiction", authorId: "1"},
//   {id: "5", name: "Book 5", genre: "fiction", authorId: "1"},
//   {id: "6", name: "Book 6", genre: "non-fiction", authorId: "2"}
// ];

const BookType = new GraphQLObjectType({
  name: "Book",
  description: "A book",
  fields: () => ({
    id: { type: GraphQLID }, 
    name: { type: GraphQLString }, 
    genre: { type: GraphQLString },
    author: {
      type: AuthorType,
      // resolve: (parent) => authors.find(author => author.id === parent.authorId)
      resolve: (parent) => Author.findById(parent.authorId)
    }
  })
});

const AuthorType = new GraphQLObjectType({
  name: "Author",
  description: "An author",
  fields: () => ({
    id: { type: GraphQLID }, 
    name: { type: GraphQLString }, 
    age: { type: GraphQLInt },
    books: {
      type: new GraphQLList(BookType),
      // resolve: (parent) => books.filter(book => book.authorId === parent.id)
      resolve: (parent) => Book.find({ authorId: parent.id })
    }
  })
});

const RootQueryType = new GraphQLObjectType({
  name: "Query",
  description: "Root Query",
  fields: () => ({

    book: {
      type: BookType,
      description: "A single book",
      args: { id: { type: GraphQLID }},
      // resolve: (parent, args) => books.find(book => book.id === args.id)
      resolve: (parent, args) => Book.findById(args.id)
    },

    books: {
      type: new GraphQLList(BookType),
      description: "List of books",
      // resolve: () => books
      resolve: () => Book.find({})
    },

    author: {
      type: AuthorType,
      description: "A single author",
      args: { id: { type: GraphQLID }},
      // resolve: (parent, args) => authors.find(author => author.id === args.id)
      resolve: (parent, args) => Author.findById(args.id)
    },

    authors:  {
      type: new GraphQLList(AuthorType),
      description: "List of authors",
      // resolve: () => authors
      resolve: () => Author.find({})
    }

  })
});

const RootMutationType = new GraphQLObjectType({
  name: "Mutation",
  description: "Root Mutation",
  fields: () => ({

    addAuthor: {
      type: AuthorType,
      description: "Add an author",
      args: {
        name: { type: GraphQLNonNull(GraphQLString) },
        age: { type: GraphQLNonNull(GraphQLInt) }
      },
      resolve: (parent, args) => {
        let author = new Author({
          name: args.name,
          age: args.age
        });
        return author.save();
      }
    },

    addBook: {
      type: BookType,
      description: "Add a book",
      args: {
        name: { type: GraphQLNonNull(GraphQLString) },
        genre: { type: GraphQLNonNull(GraphQLString) },
        authorId: { type: GraphQLNonNull(GraphQLID) }
      },
      resolve: (parent, args) => {
        let book = new Book({
          name: args.name,
          genre: args.genre,
          authorId: args.authorId
        });
        return book.save();
      }
    }

  })
});

module.exports = new GraphQLSchema({
  query: RootQueryType,
  mutation: RootMutationType
});
