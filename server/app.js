const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const { graphqlHTTP } = require("express-graphql");
const schema = require("./schema");

const app = express();

app.use(cors());

mongoose.connect("mongodb+srv://clc:clc123@cluster0.bnonl.mongodb.net/react-graphql-db?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

mongoose.connection.once("open", () => {
  console.log("Database connection is open");
});

app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    graphiql: true
  })
);

app.listen(5050, () => {
  console.log("The server is running at http://localhost:5050/graphql");
});