import React, { Component } from 'react';
import { graphql } from "react-apollo";
import { flowRight as compose } from 'lodash';
import { getAuthorsQuery, addBookMutation, getBooksQuery } from "../queries/queries";

class AddBook extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: "",
      genre: "",
      authorId: ""
    };
  }

  loadAuthors = () => {
    const data = this.props.getAuthorsQuery;

    if (data.loading) {
      return (<option disabled>Loading authors...</option>);
    }
    else {
      return data.authors.map(author => (
        <option key={author.id} value={author.id}>{author.name}</option>)
      );
    }
  };

  submitForm = (e) => {
    e.preventDefault();
    this.props.addBookMutation({
      variables: {
        name: this.state.name,
        genre: this.state.genre,
        authorId: this.state.authorId
      },
      refetchQueries: [{ query: getBooksQuery }]
    });
  };

  render() {
    return (
      <form className="add-book" onSubmit={this.submitForm}>

        <div className="field">
          <label>
            Name:
          </label>
          <input type="text" onChange={e => this.setState({ name: e.target.value })} />
        </div>

        <div className="field">
          <label>
            Genre:
          </label>
          <input type="text" onChange={e => this.setState({ genre: e.target.value })} />
        </div>

        <div className="field">
          <label>
            Author:
          </label>
          <select defaultValue="" required onChange={e => this.setState({ authorId: e.target.value })}>
            <option value="" disabled>Select an author</option>
            {this.loadAuthors()}
          </select>
        </div>

        <button>+</button>

      </form>
    );
  }

}

export default compose(
  graphql(getAuthorsQuery, { name: "getAuthorsQuery" }),
  graphql(addBookMutation, { name: "addBookMutation" })
)(AddBook);
