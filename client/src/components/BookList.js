import React, { Component } from 'react';
import { graphql } from "react-apollo";
import { getBooksQuery } from "../queries/queries";
import BookDetails from "./BookDetails";

class BookList extends Component {
  constructor(props){
    super(props);
    this.state = {
      selectedBook: null
    };
  }

  loadBooks = () => {
    const data = this.props.data;

    if (data.loading) {
      return (<div>Loading books...</div>);
    }
    else {
      return data.books.map(book => (
        <li 
          key={book.id}
          onClick={() => this.setState({selectedBook: book.id})}
        >
            {book.name}
        </li>)
      );
    }
  };

  render() {
    return (
      <div>
        <ul className="book-list">
          {this.loadBooks()}
        </ul>
        <BookDetails bookId={this.state.selectedBook} />
      </div>
    );
  }

}

export default graphql(getBooksQuery)(BookList);
